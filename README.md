# Test repo for Vagrant + Git
Welcome to Client-Side Web Crypto course.
This is the test repo for the course (tests your toolkit).

To complete the test, please run `vagrant up && vagrant ssh` and enjoy!
After completion, you should have SSH access to a local VM.
In case you have any issues, please contact the instructor!